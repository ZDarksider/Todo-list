import React, { Component } from 'react';
import {Provider} from "react-redux"

import AddTodo from './containers/AddTodo'
import VisibleTodoList from './containers/VisibleTodoList'
import Footer from './Components/Footer'
import store from './store'

class App extends Component {

  render() {
    return (
      <Provider store={store}>
        <div>
          <AddTodo />
          <VisibleTodoList />
          <Footer />
        </div>
      </Provider>
    );
  }
}

export default App;
